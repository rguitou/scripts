#!/bin/bash

function editer
{
   if [ -z $1 ] | [ $1 = "index" ]
   then
	cat markdown/help.txt
	echo -n "./blogue2.sh "
	read commande
	bash blogue2.sh $commande
   elif [ $EDITOR ]
   then
	echo $1
	local EXTENSION=$(echo '.mkd')
    	$EDITOR markdown/$1$EXTENSION
   elif [ $(find /usr/bin/editor | wc -l) != 1 ]
   then
	echo a
   else
	echo $1
	local EXTENSION=$(echo '.mkd')
    	gedit markdown/$1$EXTENSION
   fi
	
}	

function  supprimer
{
    until [ $reponse == 'o' ] | [ $reponse == 'n' ]
    do
	read -p 'En etes-vous sur ? [o/n]' reponse
	if [ $reponse == 'o' ] 
	then
		local EXTENSION=$(echo '.mkd')
		rm markdown/$1$EXTENSION
		echo Fichier Supprime
	elif [ $reponse == 'n' ]
	then
		echo Supression annulee
	fi
    done
}

function lister
{
   if [ $(ls -l markdown/*.mkd | wc -l) == 0 ]
	then
        echo "Aucun fichier"
        else
	echo liste des fichiers Markdown '(.mkd)':
	ls -l markdown/*.mkd | cut -d'/' -f2
   fi
}


function visualiser
{
	xdg-open markdown/index.html
}

function construire
{
   man find
}				

ARGUMENT=$2

case "$1" in
editer)
editer $ARGUMENT
;;
supprimer)
supprimer $ARGUMENT
;;
lister)
lister
;;
construire)
construire
;;
visualiser)
visualiser
;;
esac
