#!/bin/bash

function editer
{
	echo $1
	local EXTENSION=$(echo '.mkd')
    nano $1$EXTENSION
}	

function  supprimer
{
	local EXTENSION=$(echo '.mkd')
	rm $1$EXTENSION
}

function lister
{
	echo liste des fichiers Markdown '(.mkd)':
	
	ls -l | grep .mkd | cut -d' ' -f11
}


function visualiser
{
	xdg-open web/index.html
}				

ARGUMENT=$2

case "$1" in
editer)
editer $ARGUMENT
;;
supprimer)
supprimer $ARGUMENT
;;
lister)
lister
;;
construire)
construire
;;
visualiser)
visualiser
;;
esac
