#!/bin/bash

function editer
{
	echo $1
	local EXTENSION=$(echo '.mkd')
    nano markdown/$1$EXTENSION
}	

function  supprimer
{
	
    read -p 'En etes-vous sur ? [o/n]' reponse
    if [ $reponse == "o" ]
	then
		local EXTENSION=$(echo '.mkd')
		rm markdown/$1$EXTENSION
		echo Fichier Supprime
	else
		echo Suppression annulee
	fi
}

function lister
{
	if [ $(ls -l markdown/*.mkd | wc -l) -eq 0 ]
		then
           echo "Aucun fichier"
        else
	       echo liste des fichiers Markdown '(.mkd)':
		   ls -l markdown/*.mkd | cut -d'/' -f2
		fi
}


function visualiser
{
	xdg-open web/index.html
}				

ARGUMENT=$2

case "$1" in
editer)
editer $ARGUMENT
;;
supprimer)
supprimer $ARGUMENT
;;
lister)
lister
;;
construire)
construire
;;
visualiser)
visualiser
;;
esac
